/******************************************************************
SHORT INFORMATION
******************************************************************/
GardenerPress is compatible with popular plugins, as: WPML and qTranslate
– for building multilingual portal, SEO plugins – for better search
engine indexing.

Alethemes Team have been made a good documentation file and a also
created an Online Documentation resource, where daily the info is
updated and new tutorial (on how to use the theme) are added. We have
also a support forum on which you can get help if you have problems.

/******************************************************************
SUPPORT, ISSUES, & QUESTIONS
******************************************************************/
Support Forum - https://aletheme.com/
/*===*/
Knowledge Base - https://aletheme.com/
/*===*/
Theme's Author on ThemeForest - https://themeforest.net/user/crik0va


