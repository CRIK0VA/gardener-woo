<?php if ( is_active_sidebar( 'main-sidebar' ) ) { ?>
    <aside class="sidebar cf col-3">
        <div class="sidebar_container">
            <?php dynamic_sidebar( 'main-sidebar' ); ?>
        </div>
    </aside>
<?php } ?>