<?php


if(in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))){

	//WooCommerce theme Declarations
	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
		add_theme_support( 'woocommerce' );
	}


	//Pagination Function
	function ale_woocommerce_custom_pagination(){
		get_template_part('partials/pagination');
	}

	//Get product category
	function ale_woocommerce_category(){
		global $product;

		echo wc_get_product_category_list($product->get_id());

	}

	//Get the counter meta
	function ale_get_counter_meta(){
		if(ale_get_meta('product_counter')){
			echo ' ('.ale_get_meta('product_counter').')';
		}
	}


	//Get product description
	function ale_woocommerce_product_descr(){
		if(ale_get_meta('product_description')){
			echo ale_get_meta('product_description');
		}
	}


	//Accordion for Product
	function ale_woocommerce_product_accordion(){
		wp_enqueue_script( 'ale-woo-accordion' );

		$tabs = apply_filters( 'woocommerce_product_tabs', array() );

		if ( ! empty( $tabs ) ) : ?>
			<div id="accordion-container" class="woocommerce-tabs wc-tabs-wrapper minimal-accordion-container">
				<?php foreach ( $tabs as $key => $tab ) : ?>
					<h1 class="<?php echo esc_attr( $key ); ?>_tab">
						<?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title',
							esc_html( $tab['title'] ),
							$key ); ?>
					</h1>
					<div id="tab-<?php echo esc_attr( $key ); ?>">
						<?php call_user_func( $tab['callback'], $key, $tab ); ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif;
	}


	/*
	 * Archive Products Hooks
	 */

	//Disable Breadcrumb
	remove_action('woocommerce_before_main_content','woocommerce_breadcrumb',20);


	//Remove Default Pagination
	add_action('woocommerce_after_shop_loop','ale_woocommerce_custom_pagination',20);
	remove_action('woocommerce_after_shop_loop','woocommerce_pagination',10);


	//Remove Notices from Top Line
	remove_action( 'woocommerce_before_shop_loop','wc_print_notices', 10);

	//Add Notices before Top Line
	add_action( 'woocommerce_before_main_content','wc_print_notices', 20 );


	//Product Link manager
	remove_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_close',5 );
	add_action( 'woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_link_close',15 );


	//Add Category and counter after title
	add_action( 'woocommerce_shop_loop_item_title',create_function('','echo "<div class=\"category_top_line\">";'),15,2 );
	add_action( 'woocommerce_shop_loop_item_title','ale_woocommerce_category',15 );
	add_action( 'woocommerce_shop_loop_item_title','ale_get_counter_meta',15 );
	add_action( 'woocommerce_shop_loop_item_title',create_function('','echo "</div>";'),15,2);


	//Remove Cart from Product Item
	remove_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_add_to_cart',10 );


	//Remove Sale flash from default position
	remove_action( 'woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash',10 );

	//Add sales flash before title
	add_action( 'woocommerce_shop_loop_item_title','woocommerce_show_product_loop_sale_flash',5 );


	//Hover div data

	add_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_open',5 );
	add_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_product_title',5 );
	add_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_close',5 );

	add_action( 'woocommerce_after_shop_loop_item',create_function('','echo "<div class=\"category_top_line\">";'),10,2 );
	add_action( 'woocommerce_after_shop_loop_item','ale_woocommerce_category',10 );
	add_action( 'woocommerce_after_shop_loop_item','ale_get_counter_meta',15 );
	add_action( 'woocommerce_after_shop_loop_item',create_function('','echo "</div>";'),15,2);
	add_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_price',20 );
	add_action( 'woocommerce_after_shop_loop_item',create_function('','echo "<div class=\"hover_description\">";'),25,2 );
	add_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_open',25 );
	add_action( 'woocommerce_after_shop_loop_item','ale_woocommerce_product_descr',25 );
	add_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_close',25 );
	add_action( 'woocommerce_after_shop_loop_item',create_function('','echo "</div>";'),25,2);
	add_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_add_to_cart',30 );


	//Remove rating from archive page
	remove_action( 'woocommerce_after_shop_loop_item_title','woocommerce_template_loop_rating',5 );




	/*
	 * Single Product Page Hooks
	 */

	//Remove image and sales from first position
	remove_action( 'woocommerce_before_single_product_summary','woocommerce_show_product_sale_flash',10 );
	remove_action( 'woocommerce_before_single_product_summary','woocommerce_show_product_images',20 );

	//Add image to second position
	add_action( 'woocommerce_single_product_summary','woocommerce_show_product_images',15 );

	remove_action( 'woocommerce_single_product_summary','woocommerce_template_single_rating',10 );
	add_action( 'woocommerce_single_product_summary','woocommerce_template_single_rating',11 );

	//Wrap the price and rating
	add_action( 'woocommerce_single_product_summary',create_function('','echo "<div class=\"single_price_rating\">";'),9,2 );
	add_action( 'woocommerce_single_product_summary',create_function('','echo "</div>";'),12,2);


	//Wrap the title
	add_action( 'woocommerce_single_product_summary',create_function('','echo "<div class=\"single_top_title\">";'),4,2 );
	add_action( 'woocommerce_single_product_summary',create_function('','echo "</div>";'),12,2);

	//Wrap the cart and meta
	add_action( 'woocommerce_single_product_summary',create_function('','echo "<div class=\"single_product_meta_cart\">";'),29,2 );
	add_action( 'woocommerce_single_product_summary',create_function('','echo "</div>";'),41,2);

	//disable sharing
	remove_action( 'woocommerce_single_product_summary','woocommerce_template_single_sharing',50 );


	//Remove Tabs
	remove_action( 'woocommerce_after_single_product_summary','woocommerce_output_product_data_tabs',10 );
	add_action( 'woocommerce_after_single_product_summary','ale_woocommerce_product_accordion',10 );




	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
	add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_upsells', 15 );

	if ( ! function_exists( 'woocommerce_output_upsells' ) ) {
		function woocommerce_output_upsells() {
			woocommerce_upsell_display( 6, 3 );
		}
	}



	add_filter( 'woocommerce_output_related_products_args', 'ale_related_products_args' );
	function ale_related_products_args( $args ) {

		$args['posts_per_page'] = 6;
		$args['columns']        = 3;

		return $args;

	}



	/**
	 * Define image sizes
	 */
	function ale_woocommerce_image_dimensions() {
		global $pagenow;

		if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' ) {
			return;
		}
		$catalog = array(
			'width' 	=> '400',	// px
			'height'	=> '340',	// px
			'crop'		=> 1 		// true
		);
		$single = array(
			'width' 	=> '690',	// px
			'height'	=> '320',	// px
			'crop'		=> 1 		// true
		);
		$thumbnail = array(
			'width' 	=> '120',	// px
			'height'	=> '120',	// px
			'crop'		=> 0 		// false
		);
		// Image sizes
		update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
		update_option( 'shop_single_image_size', $single ); 		// Single product image
		update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
	}
	add_action( 'after_switch_theme', 'ale_woocommerce_image_dimensions', 1 );


/*
	// First Register the Tab by hooking into the 'woocommerce_product_data_tabs' filter
	add_filter( 'woocommerce_product_data_tabs', 'add_my_custom_product_data_tab' );
	function add_my_custom_product_data_tab( $product_data_tabs ) {
		$product_data_tabs['my-custom-tab'] = array(
			'label' => __( 'My Custom Tab', 'my_text_domain' ),
			'target' => 'my_custom_product_data',
		);
		return $product_data_tabs;
	}

	// Next provide the corresponding tab content by hooking into the 'woocommerce_product_data_panels' action hook
	// See https://github.com/woothemes/woocommerce/blob/master/includes/admin/meta-boxes/class-wc-meta-box-product-data.php
	// for more examples of tab content
	// See https://github.com/woothemes/woocommerce/blob/master/includes/admin/wc-meta-box-functions.php for other built-in
	// functions you can call to output text boxes, select boxes, etc.
	add_action( 'woocommerce_product_data_panels', 'add_mailchimp_product_data_fields' );
	function add_mailchimp_product_data_fields() {
		global $woocommerce, $post;
		?>
		<!-- id below must match target registered in above add_my_custom_product_data_tab function -->
		<div id="my_custom_product_data" class="panel woocommerce_options_panel">
			<?php
			woocommerce_wp_text_input( array(
				'id'            => 'ale_product_counter',
				'wrapper_class' => 'show_if_simple',
				'label'         => __( 'My Custom Field Label', 'my_text_domain' ),
				'description'   => __( 'My Custom Field Description', 'my_text_domain' ),
				'default'  		=> '0',
				'desc_tip'    	=> false,
			) );

			woocommerce_wp_textarea_input( array(
				'id'            => 'ale_product_description',
				'wrapper_class' => 'show_if_simple',
				'label'         => __( 'My Custom Field Label', 'my_text_domain' ),
				'description'   => __( 'My Custom Field Description', 'my_text_domain' ),
				'default'  		=> '0',
				'desc_tip'    	=> false,
			) );
			?>
		</div>
	<?php
	}

*/

	//New Styles
	function ale_woocommerce_custom_styles(){
		echo '<style type="text/css">';

		echo "

		.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt {
			background-color:#000;
		}
		.top_shop_line {
			width:100%;

			background:#ededed;
			color:#9d9d9d;
			font-family:arial, sans-serif;
			font-size:12px;
		}
		.top_shop_line .wrapper {
			display:flex;
			align-items:center;
			justify-content:space-between;
			min-height:60px;
		}
		.woocommerce .woocommerce-result-count, .woocommerce-page .woocommerce-result-count {
			float:none;
			margin:0;
		}
		.woocommerce .woocommerce-ordering, .woocommerce-page .woocommerce-ordering {
			float:none;
			margin:0;
			background:#d4d4d4;
			border-radius:30px;
			color:#9d9d9d;
			font-family:arial, sans-serif;
			padding:6px 10px;
		}
		.woocommerce .woocommerce-ordering select {
			background:transparent;
			border:none;
			color:#9d9d9d;
			font-family:arial, sans-serif;
			vertical-align: middle;
			line-height:15px;
		}
		.woocommerce ul.products, .woocommerce-page ul.products {
			display:flex;
			flex-wrap: wrap;
			justify-content: flex-start;
			width: 100%;
            border-left: 1px solid #e8e8e8;
            margin-top:55px;
            margin-bottom:55px;
		}
		.woocommerce ul.products li.product, .woocommerce-page ul.products li.product {
			margin:0;
			width:25%;
			border-right: 1px solid #e8e8e8;
		    border-bottom: 1px solid #e8e8e8;
		    border-top: 1px solid #e8e8e8;
		    text-align: center;
		    padding: 20px 24px;
		    box-sizing: border-box;
		    margin-top: -1px;
		    position: relative;
		}
		.woocommerce ul.products li:before,.woocommerce-page ul.products li:before {
			display:none;
		}
		.hover_mask {
			display:none;
			background: #FFF;
	          padding: 50px 30px 30px 30px;
	          box-sizing: border-box;
	          border-top:1px solid  #ffde5a;
	          margin-top: -10px;
	          margin-left: -10px;
	          margin-right: -10px;
	          -webkit-box-shadow: 20px 20px 104px -10px rgba(0,0,0,0.20);
	          -moz-box-shadow: 20px 20px 104px -10px rgba(0,0,0,0.20);
	          box-shadow: 20px 20px 104px -10px rgba(0,0,0,0.20);
	          z-index: 9999;
			width:100%;
			position:absolute;
			top:0;
			left:0;
			min-height:100%;
			background:#FFF;
		}
		.woocommerce ul.products li.product .hover_mask .price {
			margin-bottom:30px;
		}
		.woocommerce ul.products li.product .button {
			margin-top: 30px;
		    width: 100%;
		    height: 47px;
		    background: #ffde5a;
		    color: #36624d;
		    font-size: 18px;
		    letter-spacing: 0.1em;
		    font-weight: bold;
		    text-transform: uppercase;
		    -webkit-border-radius: 10px;
		    -moz-border-radius: 10px;
		    border-radius: 10px;
		    -moz-background-clip: padding;
		    -webkit-background-clip: padding-box;
		    background-clip: padding-box;
		    padding:0;
		    line-height:48px;
		}
		.hover_description {
			font-size:12px;
			color:#898989;
			line-height:18px;
		}
		.hover_description a {
			color:inherit;
		}
		.woocommerce ul.products li:hover .hover_mask,.woocommerce-page ul.products li:hover .hover_mask {
			display:block;
			width: calc(100% + 20px);
		}
		.woocommerce ul.products li.product .woocommerce-loop-product__title {
			font-size:18px;
			line-height:18px;
			padding:0;
			margin-bottom:7px;
		}
		.category_top_line {
			font-size:12px;
			color:#878585;
			font-family:arial, sans-serif;
			display:block;
			margin-bottom:15px;
			line-height:14px;
		}
		.category_top_line a {
			color:inherit;
		}
		.woocommerce ul.products li.product .onsale {
			top:0;
			right:0;
			left:0;
			margin:0;
			margin-top:-20px;
			line-height:10px;
			display:block;
			width:100%;
			text-align:center;
		}
		.woocommerce span.onsale {
			position:absolute;
			background:none;
			color:#207530;
			font-size:10px;
			text-transform:uppercase;
		}
		.title_section {
			position:relative;
			margin-top:20px;
		}
		.woocommerce ul.products li.product .price {
			font-size:30px;
			color:#207530;
			font-weight:700;
			margin:0;
		}
		.woocommerce ul.products li.product a img {
			margin-bottom:25px;
		}
		.woocommerce ul.products li.product .price del {
			font-size:18px;
			color:#8a8a8a;
			opacity:1;
			display:inline-block;
		}
		.woocommerce ul.products li.product .price ins {
			text-decoration:none;
		}


		/* Single Product CSS */
		/* ================== */

		.woocommerce #content div.product div.summary, .woocommerce div.product div.summary, .woocommerce-page #content div.product div.summary, .woocommerce-page div.product div.summary {
		    float: none;
		    width: 100%;
		}
		.woocommerce #content div.product div.images, .woocommerce div.product div.images, .woocommerce-page #content div.product div.images, .woocommerce-page div.product div.images {
		    float: none;
		    width: 100%;
		}
		.accordion-item{margin-bottom: 5px}
		.accordion-item-active {margin-bottom:0;}
        .accordion-item-active .accordion-header{background:#1f732f;transition:.25s;}
        .accordion-item-active .accordion-header-icon{color:#FFF;}
        .accordion-item-active .accordion-header h1{color:#FFF; font-size:24px; font-weight:700; text-transform:uppercase; }
        .accordion-header{background:#ededed;cursor:pointer;min-height:57px;line-height:57px;transition:.25s; display: flex; align-items:center;}
        .accordion-header h1{ font-size:24px;font-weight: 700;margin:0;color:#000; order:2; margin-left:10px;}
        .accordion-content{display:none;padding:12px; border-left:1px solid #f5f5f5; border-right:1px solid #f5f5f5; padding:30px 25px; border-bottom:1px solid #f5f5f5; }
        .accordion-content p {margin:0;margin-bottom:15px; color:#898989; font-size:14px; line-height:24px;}
        .accordion-content p:last-child { margin-bottom:0;}
        .accordion-header-icon{color:#000;font-size:16px;vertical-align:middle; margin-left: 25px; order:1;}
        .accordion-header-icon.accordion-header-icon-active{color:#fff;}
        .minimal-accordion-container .drawer {margin-top:-1px;}

        .minimal-accordion-container {
            margin-bottom:70px;

        }

        .woocommerce #reviews #comments ol.commentlist li {
            text-indent:0;
        }
        .woocommerce p.stars a {
            color:#1f732f;
        }
        .woocommerce #review_form #respond textarea {
            height:150px;
        }
        .up-sells h2,.related h2 {
            text-align:center;
            margin-bottom:40px;
        }
        .up-sells ul.products, .related ul.products{
            margin-top:0;
        }
        .up-sells ul.products li.product,.related ul.products li.product {
            width:33.3333%
        }



        .single_woo_product {
            margin-top:70px;
        }
        .woocommerce div.product {
            margin-right:30px;
        }
        .woocommerce div.product .product_title {
            color:#000;
            font-size:34px;
            text-transform:uppercase;
            font-weight:700;
            line-height:40px;
            max-width:70%;
            margin:0;
        }
        .single_price_rating {
            max-width:30%;
            text-align:right;
        }
        .single_top_title {
            display:flex;
            justify-content: space-between;
            margin-bottom:40px;
        }


        .woocommerce div.product .single_price_rating .price {
			font-size:30px;
			color:#207530;
			font-weight:700;
			margin:0;
			text-align:right;
			line-height:30px;
		}
		.woocommerce div.product .single_price_rating .price del {
			font-size:18px;
			color:#8a8a8a;
			opacity:1;
			display:inline-block;
		}
		.woocommerce div.product .single_price_rating .price ins {
			text-decoration:none;
		}
		.woocommerce div.product .woocommerce-product-rating {
			margin:0;
			font-size:12px;
			color:#8a8a8a;
		}
		.woocommerce div.product .woocommerce-product-rating a {
			color:inherit;
		}
		.woocommerce .star-rating span {
			color:#1f732f;
		}
		.woocommerce #content div.product div.images, .woocommerce div.product div.images, .woocommerce-page #content div.product div.images, .woocommerce-page div.product div.images {
			border:1px solid #f6f6f6;
		}
		.woocommerce div.product div.images {
			margin-bottom:30px;
		}
		.woocommerce-product-details__short-description {
			font-size:14px;
			line-height:24px;
			color:#898989;
			padding-bottom:40px;
			border-bottom:1px solid #f5f5f5;
			margin-bottom:40px;
		}
		.woocommerce-product-details__short-description p:last-child {
			margin-bottom:0;
		}
		.single_product_meta_cart {
			display:flex;
			flex-direction: row-reverse;
			justify-content: space-between;
			align-items: center;
			margin-bottom:55px;
		}
		.sku_wrapper, .posted_in, .tagged_as {
			display:block;
			font-size:14px;
			color:#898989;
			font-weight:bold;
		}
		.product_meta a {
			color:#1f732f;
			text-decoration:underline;
			font-weight:normal;
		}
		.sku {
			font-weight:normal;
		}
		.woocommerce div.product form.cart {
			margin-bottom:0;
		}
		.woocommerce .quantity .qty {
			width:40px;
			padding-left:10px;
			padding-right:10px;
			height:46px;
			border:1px solid #f5f5f5;
			border-radius:10px;
			font-size:14px;
			color:#737373;
			padding-top:0;
			padding-bottom:0;
		}
		.woocommerce div.product form.cart div.quantity {
			margin-right:8px;
		}
		.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt {
		    height: 48px;
		    background: #ffde5a;
		    color: #36624d;
		    font-size: 18px;
		    letter-spacing: 0.1em;
		    font-weight: bold;
		    text-transform: uppercase;
		    -webkit-border-radius: 10px;
		    -moz-border-radius: 10px;
		    border-radius: 10px;
		    -moz-background-clip: padding;
		    -webkit-background-clip: padding-box;
		    background-clip: padding-box;
		    padding:0 30px;
		    line-height:48px;
		}
		.woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover {
			background-color:#333;
		}



		/*
			Sidebar
		*/

		.woocommerce ul.cart_list li a, .woocommerce ul.product_list_widget li a {
			color:inherit;
			font-size:14px;
			line-height:18px;
			text-transform:uppercase;
		}
		aside.sidebar ul li:before {
			display:none!important;
		}
		aside.sidebar ul li .quantity {
			color:#898989;
			font-size:14px;
		}

		.woocommerce aside.sidebar .widget {
			margin-bottom:40px;
		}

		.woocommerce aside.sidebar .widget .star-rating {
			font-size: 0.8em;
			margin-bottom: 5px;
            margin-top: 5px;

		}

		.woocommerce aside.sidebar .widget_products .woocommerce-Price-amount {
			font-size:14px;
			color:#1f732f;
		}
		.woocommerce aside.sidebar .widget_top_rated_products .woocommerce-Price-amount {
			font-size:14px;
			color:#898989;
		}
		.woocommerce ul.product_list_widget li {
			border-bottom:1px solid #f5f5f5;
			padding:0;
			margin:0;
			padding-bottom:10px;
			margin-bottom:10px;
			line-height:14px;

		}
		.woocommerce ul.cart_list li img, .woocommerce ul.product_list_widget li img {
			border:1px solid #f5f5f5;
		}
		.woocommerce .widget_shopping_cart .cart_list li a.remove, .woocommerce.widget_shopping_cart .cart_list li a.remove {
		    position: absolute;
		    top: -3px;
		    left: 0;
		    font-size: 21px;
		}
		.woocommerce .widget_shopping_cart .total, .woocommerce.widget_shopping_cart .total {
			border:0;
			text-align:center;
			color:#000;
			font-size:14px;
			text-transform:uppercase;
			background:#f5f5f5;
			height:45px;
			line-height:45px;
			border-radius:10px;
			font-weight:700;
			padding:0;
		}
		.woocommerce .widget_shopping_cart .total strong, .woocommerce.widget_shopping_cart .total strong {
			color:#898989;
			font-weight:400;
		}
		.woocommerce .widget_shopping_cart .buttons a, .woocommerce.widget_shopping_cart .buttons a {
			margin:0;
			width:50%;
			box-sizing:border-box;
		}

		.woocommerce .widget_shopping_cart .buttons a.button,.woocommerce.widget_shopping_cart .buttons a.button {
			height: 48px;
		    background: #ffde5a;
		    color: #36624d;
		    font-size: 14px;
		    letter-spacing: 0;
		    font-weight: bold;
		    text-transform: uppercase;
		    border-radius:0;
		    -webkit-border-top-left-radius: 10px;
			-webkit-border-bottom-left-radius: 10px;
			-moz-border-radius-topleft: 10px;
			-moz-border-radius-bottomleft: 10px;
			border-top-left-radius: 10px;
			border-bottom-left-radius: 10px;
		    -moz-background-clip: padding;
		    -webkit-background-clip: padding-box;
		    background-clip: padding-box;
		    text-align:center;
		    padding:0;
		    line-height:48px;
		}
		.woocommerce .widget_shopping_cart .buttons a.button.checkout,.woocommerce.widget_shopping_cart .buttons a.button.checkout {
			border-radius:0;
			-webkit-border-top-right-radius: 10px;
			-webkit-border-bottom-right-radius: 10px;
			-moz-border-radius-topright: 10px;
			-moz-border-radius-bottomright: 10px;
			border-top-right-radius: 10px;
			border-bottom-right-radius: 10px;
			border-left:1px solid #e5c751;
		}

		#add_payment_method table.cart td.actions .coupon .input-text, .woocommerce-cart table.cart td.actions .coupon .input-text, .woocommerce-checkout table.cart td.actions .coupon .input-text {
			padding:0 15px;
			height:48px;
		}
		.woocommerce table.cart td.actions {
			padding:20px;
		}
		.woocommerce #content table.cart td.actions .input-text, .woocommerce table.cart td.actions .input-text, .woocommerce-page #content table.cart td.actions .input-text, .woocommerce-page table.cart td.actions .input-text {
			width:150px;
		}
		.woocommerce input.button {
			height:48px;
		}
		.woocommerce-cart-form {
			margin-bottom:50px;
		}
		.woocommerce .cart-collaterals .cross-sells, .woocommerce-page .cart-collaterals .cross-sells {
			width:100%;
			float:none;
		    margin-left: -110px;
            margin-right: -110px;
            width: calc(100% + 220px);
		}
		.woocommerce .cart-collaterals .cross-sells h2, .woocommerce-page .cart-collaterals .cross-sells h2,
		 .woocommerce .cart-collaterals .cart_totals h2, .woocommerce-page .cart-collaterals .cart_totals h2 {
			text-align:center;
		}
		.woocommerce .cart-collaterals .cross-sells ul.products li.product, .woocommerce-page .cart-collaterals .cross-sells ul.products li.product {
			width:25%;
		}
		.woocommerce .cart-collaterals .cart_totals, .woocommerce-page .cart-collaterals .cart_totals {
			width:100%;
			float:none;
		}
		.site_container .single_post .post_content_section .hover_description a {color:inherit;}
		.site_container .single_post .post_content_section .woocommerce  a {
			text-decoration: none;
		}
		.woocommerce-page .site_container .single_post .post_title_section .post_info {
			display:none;
		}
		.woocommerce form .form-row textarea {
			width: 100%;
			  height: 158px;
			  box-sizing: border-box;
			  border: 0;
			  -webkit-border-radius: 3px;
			  -moz-border-radius: 3px;
			  border-radius: 3px;
			  -moz-background-clip: padding;
			  -webkit-background-clip: padding-box;
			  background-clip: padding-box;
			  font-size: 12px;
			  color: #898989;
			  padding: 20px 25px;
			  -webkit-box-shadow: inset 0px 3px 9px 0px rgba(0, 0, 0, 0.25);
			  -moz-box-shadow: inset 0px 3px 9px 0px rgba(0, 0, 0, 0.25);
			  box-shadow: inset 0px 3px 9px 0px rgba(0, 0, 0, 0.25);
		}
		.woocommerce form .form-row textarea::-webkit-input-placeholder {
		  /* Chrome/Opera/Safari */
		  font-size: 12px;
		  color: #898989;
		}
		.story textarea::-moz-placeholder,
		.order_form_data textarea::-moz-placeholder {
		  /* Firefox 19+ */
		  font-size: 12px;
		  color: #898989;
		}
		.woocommerce form .form-row textarea:-ms-input-placeholder {
		  /* IE 10+ */
		  font-size: 12px;
		  color: #898989;
		}
		.woocommerce form .form-row textarea:-moz-placeholder {
		  /* Firefox 18- */
		  font-size: 12px;
		  color: #898989;
		}
		#customer_details {
			margin-bottom:50px;
		}
		.woocommerce table.shop_table {
			border-collapse: collapse;
			border:inherit;
			border-radius: 15px;
			margin: 0 0 50px 0;
		}
		#add_payment_method #payment, .woocommerce-cart #payment, .woocommerce-checkout #payment {
			border-radius: 15px;
		}
		.story .checkout_coupon  input[type=\"text\"].input-text {
			height:48px;
		}
		.checkout_coupon .button {
			width:100%
		}
		";

		echo '</style>';
	}

	add_action('wp_head','ale_woocommerce_custom_styles');

}
